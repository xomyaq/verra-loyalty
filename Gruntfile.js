module.exports = function (grunt) {
  "use strict";

  grunt.initConfig({
    meta: {
      now: Date.now()
    },
    jade: {
      debug: {
        options: {
          debug: true,
          pretty: true
        },
        files: [{
          expand: true,
          cwd: "views",
          src: "*.jade",
          dest: ".",
          ext: ".html"
        }]
      },
      release: {
        options: {
          debug: false,
          data: {
            commit: grunt.option("commit")
          }
        },
        files: [{
          expand: true,
          cwd: "views",
          src: "*.jade",
          dest: ".",
          ext: ".html"
        }]
      }
    },
    stylus: {
      debug: {
        options: {
          compress: false
        },
        files: {
          "css/style.css": "css/style.styl"
        }
      },
      release: {
        options: {
          compress: true
        },
        files: {
          "css/style.css": "css/style.styl"
        }
      }
    },
    watch: {
      debug: {
        options: {
          livereload: true
        },
        files: ["views/**/*.jade", "css/*.styl", "js/**/*.js"],
        tasks: ["jade:debug", "stylus:debug"]
      }
    },
    compress: {
      default: {
        options: {
          archive: "verra-loyalty.zip"
        },
        files: [
          { expand: true, src: ["css/*.css"], dest: "." },
          { expand: true, src: ["i/**"], dest: "." },
          { expand: true, src: ["js/**"], dest: "." },
          { expand: true, src: ["index.html"], dest: "." }
        ]
      }
    }
  });

  grunt.loadNpmTasks("grunt-contrib-jade");
  grunt.loadNpmTasks("grunt-contrib-stylus");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-contrib-compress");

  grunt.registerTask("default", ["jade:debug", "stylus:debug", "watch:debug"]);
  grunt.registerTask("dev", ["jade:debug", "stylus:debug"]);
  grunt.registerTask("release", ["jade:release", "stylus:release"]);
  grunt.registerTask("build", ["jade:release", "stylus:release", "compress"]);
};
