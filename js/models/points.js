define(function () {
  "use strict";

  var EventModel = function (data) {
    var date = new Date(data.time * 1000)
      , day = date.getDate() > 9 ? date.getDate().toString() : "0" + date.getDate().toString()
      , month = date.getMonth() > 8 ? (date.getMonth() + 1).toString() : "0" + (date.getMonth() + 1).toString()
      , year = date.getFullYear();

    this.title = data.title;
    this.points = parseInt(data.points, 10);
    this.time = [day, month, year].join(".");
    this.eventType = data.event_type;
    this.amount = parseInt(data.amount, 10);
    this.statusScore = parseInt(data.status_score, 10);
    this.total = data.total || 0;
    this.totalBall = data.totalBall || 0;
  };

  return EventModel;
});
