define(function () {
  "use strict";

  var NewsModel = function (data) {
    this.id = data.id;
    this.link = data.content;
    this.description = data.preview;
    this.title = data.title;
    this.type = data.type;
  };

  return NewsModel;
});
