define([
  "jquery",
  "lodash",
  "knockout",
  "api",
  "models/news",
  "models/points",
  "cookie"
], function ($, _, ko, api, News, Event) {
  "use strict";

  var statuses = ["classic", "premium", "individual"]
    , statusSteps = [0, 10000000, 35000000];

  var ViewModel = function () {
    var self = this;

    self.token = ko.observable("");

    self.userName = ko.observable("");

    self.userPhone = ko.observable("");

    self.userEmail = ko.observable("");

    self.cardPoints = ko.observable(0);

    self.cardStatus = ko.observable("classic");

    self.statusScore = ko.observable(0);

    self.nextStatusScore = ko.computed(function () {
      return statusSteps[_.indexOf(statuses, self.cardStatus()) + 1] - self.statusScore();
    });

    self.eventList = ko.observableArray([]);

    self.newsList = ko.observableArray([]);

    self.pointEvents = ko.computed(function () {
      var eventList = self.eventList()
        , total = 0
        , totalBall = 0
        , groupedEvents = {};

      eventList = _(eventList).forEach(function (event, index) {
        total += parseInt(event.points, 10);
        totalBall += parseInt(event.status_score, 10);
      }).map(function (event, index) {
        event.total = total -= parseInt(index !== 0 ? eventList[index - 1].points: 0);
        event.totalBall = totalBall -= parseInt(index !== 0 ? eventList[index - 1].status_score: 0);
        if (typeof groupedEvents[event.doc_id] === "undefined") {
          groupedEvents[event.doc_id] = {
            title: event.title,
            points: parseInt(event.points),
            time: event.time,
            event_type: event.event_type,
            amount: parseInt(event.amount),
            status_score: parseInt(event.status_score),
            total: event.total,
            totalBall: event.totalBall
          };
        } else {
          groupedEvents[event.doc_id].points += parseInt(event.points);
          groupedEvents[event.doc_id].amount += parseInt(event.amount);
          groupedEvents[event.doc_id].status_score += parseInt(event.status_score);
        }

        return event;
      }).valueOf();

      return _.map(groupedEvents, function (eventData) { return new Event(eventData)});
    });

    self.news = ko.computed(function () {
      return _.map(self.newsList(), function (data) {
        return new News(data);
      });
    });

    self.page = ko.observable("description");

    self.login = function (form) {
      var formData = $(form).serializeArray();

      api.login(formData[0].value, formData[1].value)
        .done(function (res) {
          var token = res.response;

          self.token(token);
          $.cookie("verra_auth_token", token);
        })
        .fail(function (err) {
          console.error(err.responseText);
        });
    };

    self.getPointsInfo = function (token) {
      api.getPointsInfo(token)
        .done(function (res) {
          self.cardPoints(res.response.cur_points);
          self.eventList(res.response.event_list);
        })
        .fail(function (err) {
          console.error(err.responseText);
        });
    };

    self.getStatusInfo = function (token) {
      api.getStatusInfo(token)
        .done(function (res) {
          var data = res.response;

          self.statusScore(data.status_score);
          self.cardStatus(statuses[data.card_status - 1]);
        })
        .fail(function (err) {
          console.error(err.responseText);
        });
    };

    self.getUserInfo = function (token) {
      api.getUserInfo(token)
        .done(function (res) {
          var data = res.response;

          self.userName(data.fio);
          self.userPhone(data.phone);
          self.userEmail(data.email || "example@mail.com");
        })
        .fail(function (err) {
          console.error(err.responseText);
        });
    };

    self.getContent = function () {
      api.getContent()
        .done(function (res) {
          self.newsList(res.response);
        })
        .fail(function (err) {
          console.error(err.responseText);
        });
    };

    self.showNews = function (data, event) {
      var $el = $(event.target);

      if (!$el.hasClass("active")) {
        $el.addClass("active");
        $el.after('<iframe id="news-frame-' + data.id + '" src=' + data.link + ' width="100%" height="300"></iframe>');
      } else {
        $el.removeClass("active");
        $("#news-frame-" + data.id).remove();
      }
    };

    ko.computed(function () {
      var token = self.token();

      if (token.length) {
        self.getPointsInfo(token);
        self.getUserInfo(token);
        self.getStatusInfo(token);
      }
    });

    ko.computed(function () {
      self.getContent();
    });
  };

  return ViewModel;
});
