define([
  "jquery",
  "md5"
], function ($, md5) {
  "use strict";

  $.support.cors = true;

  var ajax = function (data) {
    var params = {
      url: "http://mobile.toyota59.ru/",
      type: "post",
      dataType: "json",
      data: JSON.stringify(data)
    };

    return $.ajax(params);
  };

  return {
    getPointsInfo: function (token) {
      return ajax({
        act: "points_info",
        auth_token: token
      });
    },
    getStatusInfo: function (token) {
      return ajax({
        act: "status_info",
        auth_token: token
      });
    },
    getContent: function () {
      return ajax({
        act: "get_content",
        cnt_type: "s"
      });
    },
    getUserInfo: function (token) {
      return ajax({
        act: "user_info",
        auth_token: token
      });
    },
    login: function (login, hash) {
      return ajax({
        act: "login",
        login: login,
        pass_hash: md5(hash)
      });
    }
  };
});
