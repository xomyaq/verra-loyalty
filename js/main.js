require.config({
  urlArgs: "bust=" +  (new Date()).getTime(),
  baseUrl: "js",
  paths: {
    jquery: "libs/jquery-1.10.2.min",
    lodash: "libs/lodash.compat.min",
    knockout: "libs/knockout-2.3.0",
    cookie: "libs/jquery.cookie.min",
    "jquery.collapse": "libs/jquery.collapse",
    "jquery.collapseStorage": "libs/jquery.collapse_storage",
    md5: "libs/md5"
  },
  shim: {
    jquery: {
      exports: "$"
    },
    lodash: {
      exports: "_"
    },
    knockout: {
      exports: "ko"
    },
    cookie: ["jquery"],
    "jquery.collapse": {
      deps: ["jquery"],
      exports: "jQuery.fn.collapse"
    },
    "jquery.collapseStorage": {
      deps: ["jquery", "jquery.collapse"],
      exports: "jQuery.fn.collapse.cookieStorage"
    },
    md5: {
      exports: "md5"
    }
  }
});

require([
  "jquery",
  "knockout",
  "viewmodel",
  "jquery.collapse",
  "jquery.collapseStorage"
], function ($, ko, ViewModel) {
  ko.applyBindings(new ViewModel());

  $("#partner_box").collapse({
    accordion: true,
    query: ".partner_wrap span",
    open: function() {
      this.slideDown(150);
    },
    close: function() {
      this.slideUp(150);
    }
  });
  $("#statusbox").collapse({
    accordion: true,
    query: ".partner_wrap .collaps",
    open: function() {
      this.slideDown(150);
    },
    close: function() {
      this.slideUp(150);
    }
  });
  $(document).on("click", "#clck_priv", function (e) {
    e.preventDefault();
    $("#clck_block").slideToggle(200);
  });
  $(document).on("click", ".partner_img a", function (e) {
    $("#overlay").addClass("active");
    $(this).parent().find(".partner_item").slideDown(300);
  });
  $(document).on("click", "img.box_close", function (e) {
    $(this).parent().parent().slideUp(300);
    $("#overlay").removeClass("active");
  });
});
